A simple tool to download a single user's mattermost message history via the API. Unlike other mattermost export tools, 
this works without an enterprise license or access to the database.

## Usage
1. Download `mattermost-downloader.exe` from [Releases](https://gitlab.com/mhamiltonj/mattermost-downloader/-/releases)
1. Create a Personal Access Token: Click on you're user avatar -> Profile -> Security -> Personal Access Tokens ->
Create Token. Copy the resulting value
2. Run `mattermost-downloader.exe`
3. Right click to paste your PAT when prompted.
4. Enter the URL of your mattermost server (e.g. https://chat.my-company.com)
5. Enter the location where you want the files with your messages to be stored. 

## Output
The tool will create a folder called `mattermost-export-Your Full-Name` inside the at the location you specify. In this 
folder it will create 2 files for each channel (group and direct message) that you have participated in:
One JSON file with the full output returned by the Mattermost API ([see the docs](https://api.mattermost.com/#tag/posts/operation/GetPostsForChannel)) 
and one CSV file with a list of massages formatted as: `sent time; edited time; sending user; message`