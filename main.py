import requests
from getpass import getpass
from datetime import datetime
import csv
import json
import os
import traceback

global known_users
known_users = {}


def get_user(user_id, url, token):
    headers = {'Authorization': f'Bearer {token}'}

    if user_id in known_users:
        full_name = known_users[user_id]
    else:
        r = requests.get(f'{url}/api/v4/users/{user_id}', headers=headers)
        full_name = f'{r.json()["first_name"]} {r.json()["last_name"]}'

        known_users[user_id] = full_name

    return full_name


def get_channel_messages(channel_id, url=..., token=...):
    headers = {'Authorization': f'Bearer {token}'}

    messages = []
    page = 0
    last_page = False
    while not last_page:
        r = requests.get(f'{url}/api/v4/channels/{channel_id}/posts?per_page=200&page={page}', headers=headers)

        for post_id in r.json()['order']:
            post = r.json()['posts'][post_id]
            messages.append((datetime.utcfromtimestamp(post['create_at']/1000),
                             datetime.utcfromtimestamp(post['edit_at']/1000) if post['edit_at'] != 0 else None,
                             get_user(post['user_id'], url, token), post['message']))

        if len(r.json()['posts']) < 200:
            last_page = True
        else:
            page += 1

    return r.json(), messages


def get_channels(user_id, url, token):
    headers = {'Authorization': f'Bearer {token}'}
    r = requests.get(f'{url}/api/v4/users/me/channels?per_page=1000', headers=headers)

    out = {}
    for  channel in r.json():

        if channel['display_name'] == '':
            users = channel['name'].split('__')
            name = get_user(users[0], url, token) + '__' + get_user(users[1], url, token) + channel['id']
        else:
            name = channel['display_name'] + '_' + channel['id']
        out[channel['id']] = name.replace('/', '_')

    return out


if __name__ == '__main__':
    try:
        token = getpass('Enter your Personal Access Token (right click to paste, input will not be shown): ')
        url = input('Mattermost Instance URL (including http/https) ')
        store_dir = input('Enter a path to the directory where exports should be stored: ')

        if len(token) < 16:
            raise ValueError('Your Personal Access Token is expected to be at least 16 characters long. Please not that '
                             'this is not your password. Use a single right click to paste it at the prompt, Ctrl+V will'
                             'not work')

        headers = {'Authorization': f'Bearer {token}'}
        r = requests.get(f'{url}/api/v4/users/me', headers=headers)
        if r.status_code != 200:
            raise ConnectionError(r.content)
        user_full_name = f'{r.json()["first_name"]} {r.json()["last_name"]}'
        user_id = r.json()['id']
        known_users[user_id] = user_full_name

        store_dir = os.path.join(store_dir, f'mattermost-export-{user_full_name}')
        os.mkdir(store_dir)

        for c_id, channel in get_channels(user_id, url, token).items():
            print(channel)
            j, m = get_channel_messages(c_id, url, token=token)
            file_name = channel.replace(', ', '-').replace('.', '_')
            csv_file_name = f"{file_name}.csv"
            json_file_name = f"{file_name}.json"
            with open(os.path.join(store_dir, csv_file_name), 'w', newline='', encoding='utf-8') as csvfile:
                writer = csv.writer(csvfile, delimiter=';')
                writer.writerows(m)

            with open(os.path.join(store_dir, json_file_name), 'w', newline='', encoding='utf-8') as jsonfile:
                json.dump(j, jsonfile)

        print('DONE!')
        os.system("pause")
    except Exception as e:
        print(traceback.format_exc())
        os.system("pause")
